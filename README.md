# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://freakstein@bitbucket.org/freakstein/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/freakstein/stroboskop/commits/ceed463

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/freakstein/stroboskop/commits/0cb2bb5

Naloga 6.3.2:
https://bitbucket.org/freakstein/stroboskop/commits/228681c

Naloga 6.3.3:
https://bitbucket.org/freakstein/stroboskop/commits/ded0464

Naloga 6.3.4:
https://bitbucket.org/freakstein/stroboskop/commits/0710d17

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/freakstein/stroboskop/commits/90d4c65

Naloga 6.4.2:
https://bitbucket.org/freakstein/stroboskop/commits/b6425a8

Naloga 6.4.3:
https://bitbucket.org/freakstein/stroboskop/commits/58218e8

Naloga 6.4.4:
https://bitbucket.org/freakstein/stroboskop/commits/2b96796